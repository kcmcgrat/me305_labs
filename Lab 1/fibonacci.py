'''@file fibonacci.py
@brief   Fibonacci program
@details Asks the user to input an integer and then prints out the 
         fib number with that integer as the index
         Source code: https://bitbucket.org/kcmcgrat/me305_labs/src/master/Lab%201/fibonacci.py
@author  Kyle McGrath
@date    1/21/21
'''


def fib(idx):
    '''
    @brief    This function calculates a Fibonacci number at a specific index.
    @param    idx An integer specifying the index of the desired Fibonacci number
    @return   fib An integer containing the desired Fibonacci number.
    '''
    f0 = 0
    f1 = 1
    n = 1
    fib = 0
    if idx == 0:
        return 0
    while (n != idx):
        temp = f0
        f0 = f1
        f1 = f1 + temp
        n += 1
    fib = f1
    return fib


if __name__ =='__main__':
    while True:
        ##@brief Ask the user to enter an index. 
        user_input = input('Please enter an index: ')
        while not user_input.isnumeric() or int(user_input) < 0 :
             print('Please input a valid index')
             user_input = input('Please enter an index: ')
        ##@brief Convert the user's input to in integer
        idx = int(user_input)
        print ('Fibonacci number at '
               'index {:} is {:}.'.format(idx,fib(idx)))
        ##@brief The user types Q to quit or any other key to run again
        last_input = input('Type Q to quit or any key to select another index: ')
        if last_input == 'Q':
            print("Goodbye")
            break
                 
    




    
