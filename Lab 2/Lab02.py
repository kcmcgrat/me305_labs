'''@file    Lab02.py
@brief      FSM that cycles through three LED patterns when a button a pressed
@details    Implements a finite state machine shown below, that prompts the 
            user to push a button and cycles through a square LED pattern, 
            a sinewave LED pattern, and a tooth LED pattern upon each 
            subsequent button press. 
            
            Full code can be found here: 
            https://bitbucket.org/kcmcgrat/me305_labs/src/master/Lab%202/Lab02.py
                
            See code demo here: https://youtu.be/fBh0aeUahUA
                
            State=Transition Diagram Shown Below:
@image      html Lab02Diagram.jpg State-transition diagram for the system
    
@author     Kyle McGrath
@date       2/4/21
@copyright  License Info Here
'''

import pyb 
import utime
import math



def onButtonPress(IRQ_SRC):
    '''
    @brief    Function to change state button press
    @details  Changes the value of global variable buttonPushed to 1 when the 
              user presses a button. 
    @param    IRQ_SRC Interrupt request. 
    '''
    global buttonPushed
    buttonPushed = 1



if __name__ == "__main__":
    #Program initialization goes here
    ##@brief Variable containing the current state, starting at state 0. 
    state = 0
    ##@brief Variable containing the time when a button is pushed. 
    pushTime = 0
    ##@brief Variable containing the current button status. 
    #@details Contains 1 when button is pressed, 0 when not pressed. 
    buttonPushed = 0 
    print("Welcome. Press the user button B1 on the Nucleo to cycle through LED Patterns")
    ##@brief Variable containing the LED pin.
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    ##@brief Variable containing the timer setup.
    tim2 = pyb.Timer(2, freq = 20000)
    ##@brief Configures the PWM channel
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    ##@brief Variable to store the button pin
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ##@brief Detects when the button is pressed. 
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                                       pull=pyb.Pin.PULL_NONE, callback=onButtonPress)
  
    while True:
        try:
            if state == 0:
                if buttonPushed:
                    state = 1
                    buttonPushed = 0
                    print("Square pattern selected")
                    pushTime = utime.ticks_ms()
                
            elif state == 1:
                ##@brief Variable containing the reference time.
                #@details The current time in order to determine how much 
                #         time has passed since the button was pressed.
                refTime = utime.ticks_ms()
                ##@brief Time difference between reference time and button push
                diff = utime.ticks_diff(refTime, pushTime)
                 
                if diff <= 500:
                    t2ch1.pulse_width_percent(100)
                elif diff < 1000 and diff > 500:
                    t2ch1.pulse_width_percent(0)
                else:
                    t2ch1.pulse_width_percent(100)
                    pushTime = utime.ticks_ms()
                
                
                if buttonPushed:
                    state = 2
                    buttonPushed = 0
                    print("Sinewave pattern selected")
                    pushTime = utime.ticks_ms()
                
                
            elif state == 2:
                refTime= utime.ticks_ms()
                diff = utime.ticks_diff(refTime, pushTime)
                ##@brief Variable to calculate the value of the sine wave
                pulse = 1 / 2 * math.sin(math.pi / 5000 * diff) + 0.5
                ##@brief Convert sin wave value to a percentage. 
                pulsePercent = pulse * 100
                t2ch1.pulse_width_percent(pulsePercent)
               
                
                if buttonPushed:
                    state = 3
                    buttonPushed = 0
                    print("Tooth pattern selected")
                    pushTime = utime.ticks_ms()
               
                
            elif state == 3:
              
                refTime= utime.ticks_ms()
                diff = utime.ticks_diff(refTime, pushTime)
                ##@brief Convert time in ms to seconds.
                time = diff / 1000
                ##@brief Calculate duty 
                duty = 100 * (time % 1)
              
                if diff <= 1000:
                    t2ch1.pulse_width_percent(duty)
                
                else:
                    t2ch1.pulse_width_percent(0)
                    pushTime = utime.ticks_ms()
                
                if buttonPushed:
                    state = 1
                    buttonPushed = 0
                    print("Square pattern selected")
                    pushTime = utime.ticks_ms()
                
       
            else:
                pass
         
        except KeyboardInterrupt:
            #This except block catches "Ctrl-C" from the keyboard to end the
            #while(True) loop when desired
            break
        
    #Program de-initilization goes here 
