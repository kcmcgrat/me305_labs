'''@file    ElevatorFSM.py
@brief      Simulates a Two Floor Elevator
@details    Implements a finite state, machine shown below, to simulate
            the behavior of a two floor elevator 
            Source Code Available Here: https://bitbucket.org/kcmcgrat/me305_labs/src/master/HW%202/ElevatorFSM.py
@author     Kyle McGrath
@date       1/27/21
@copyright  License Info Here
'''

import time
import random


def motor_cmd(cmd):
    '''
    @brief    Commands the motor to move, down, or stop.
    @param    cmd A 0, 1, or 2 that respectively tells the motor to stop, move up, or move down
    '''
    if cmd==1:
        print('Motor Up')
    elif cmd==0:
        print('Motor Stop')
    elif cmd==2:
        print('Motor Down')
        
def button_cmd(cmd, button):
    '''
    @brief    Prints the results of of button_1 in order to visualize button presses.
    @param    cmd A 0 or 1 corresponding to the whether the button is not pressed (0) or pressed (1).
    @param    button Used to tell the string command what button to print
    '''
    if cmd==1:
        print('Button %d pushed' % button)
    elif cmd==0:
        print('Button %d cleared' % button)
  
        
def button_1():
    '''
    @brief   Simulates the user pressing button 1
    '''
    return random.choice([True, False]) #randomly returns T or F
def button_2():
    '''
    @brief   Simulates the user pressing button 2
    '''
    return random.choice([True, False]) #randomly returns T or F
def first(iteration):
    '''
    @brief   Simulates the sensor detecting the elevator on second floor 
    @details Checks to see if 3 iterations have passed in order to simulate the distance between the two floors. 
    '''
    if iteration == 3:
        return 1
    else:
        return 0
def second(iteration):
    '''
    @brief   Simulates the sensor detecting the elevator on second floor 
    @details Checks to see if 3 iterations have passed in order to simulate the distance between the two floors. 
    '''
    if iteration == 3:
        return 1
    else:
        return 0

#this code only runs if the script is executed as main by pressing play
#but does not run if the script is imported as a module 
if __name__ == "__main__":
    #Program initialization goes here
    ##@brief The current state.
    state = 0  # Initial state is the init state 
    ##@brief Tracks the iterations in order to simulate distance between floors.
    iteration = 0 
    while True:
        try:
            #main program code goes here
            if state == 0:
                print('\nS0: Moving Down')
                if first(iteration):
                    print('First Floor Landing.')
                    motor_cmd(0)
                    button_cmd(0, 1)
                    state = 1
                    iteration = 0
                else:
                    iteration += 1
                #run state 0 code
            elif state == 1:
                print('\nS1: Stopped on Floor 1')
                if button_1():
                    button_cmd(1, 1)
                    button_cmd(0, 1)
    
                elif button_2():
                    button_cmd(1, 2)
                    motor_cmd(1)
                    state = 2
                    
            elif state == 2:
                print('\nS2: Moving Up')
                #if go button is pressed, start motor and transition to S3
                if second(iteration):
                    print('Second Floor Landing')
                    motor_cmd(0)
                    button_cmd(0, 2)
                    state = 3
                    iteration = 0
                else:
                    iteration += 1
                
            elif state == 3:
                print('\nS3: Stopped on Floor 2')
                if button_2():
                    button_cmd(1, 2)
                    button_cmd(0, 2)
                    
                elif button_1():
                    button_cmd(1,1)
                    motor_cmd(2)
                    state = 0
                
   
            else:
                pass
                #code to run if state number is invalid
                #program should ideally never reach here 
                
            #Slow down execution of FSM so we can see output in console
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            #This except block catches "Ctrl-C" from the keyboard to end the
            #while(True) loop when desired
            break
        
    #Program de-initilization goes here 
