'''@file    morse.py
@brief      FSM to run the SimonSays morse code game. 
@details    Implements a finite state machine shown below, that creates a 
            pattern, displays that pattern, gets the users input, then 
            compares the results. 
            
            Full code can be found here: 
            https://bitbucket.org/kcmcgrat/me305_labs/src/master/Lab%203/morse.py
                
            See code demo here: https://youtu.be/tEBZcSGxg34
                
            State=Transition Diagram Shown Below:
@image      html Lab03Diagram.jpg State-transition diagram for the system
    
@author     Kyle McGrath
@date       2/18/21
@copyright  License Info Here
'''

import random
import utime
import pyb

class Morse:
    
    def onButtonPress(self, IRQ_SRC):
        '''
        @brief    Function to change state button press
        @details  Changes the value of global variable buttonPushed to 1 when the 
                  user presses a button. 
        param    IRQ_SRC Interrupt request. 
        '''
        
        self.buttonPushed = 1
    
    def __init__(self):
        '''
        @brief    Initialize the Morse class. 
      
        '''
        
        ##@brief Variable containing the state.
        self.state = 0
        ##@brief Variable containing the time to compare the reference time to.
        self.pushTime = 0
        ##@brief Variable containing the rising press of the button.
        self.upTime = 0
        ##@brief Variable containing the falling press of the button.
        self.downTime = 0
        ##@brief Variable containing the current button status. 
        #@details Contains 1 when button is pressed, 0 when not pressed. 
        self.buttonPushed = 0
        ##@brief The generated pattern will be stored here. 
        self.pattern = []
        ##@brief A copy of the pattern for popping purposes
        self.patternCopy = []
        ##@brief The users solution will be stored here
        self.userSoln = []
        ##@brief The current difficulty corresponding to the number of actions
        self.difficulty = 3 
        ##@brief The number of wins.
        self.wins = 0
        ##@brief The number of losses.
        self.losses = 0
        ##@brief Variable containing the LED pin.
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        ##@brief Variable containing the timer setup.
        self.tim2 = pyb.Timer(2, freq = 20000)
        ##@brief Configures the PWM channel
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        ##@brief Variable to store the button pin
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        ##@brief Detects when the button is pressed. 
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                                       pull=pyb.Pin.PULL_NONE, callback=self.onButtonPress)
       
    
    def run(self):
        '''
        @brief    Function that runs when the state machine runs. 
        @details  Cycles through the states of the finite state machine. 
        '''
    
        if self.state==0:
            # run state 0 (init) code
            self.pattern = self.createPattern(self.difficulty) # Command motor to go forward
            #print(self.pattern)
            self.patternCopy = self.pattern.copy()
            print('Round has started.')
            
            self.transitionTo(1)
                
        elif self.state==1: 
            ##@brief The current popped item of the patternCopy
            current = self.patternCopy.pop(0)
            ##@brief A reference time for use in utime.ticks_diff
            startTime = utime.ticks_ms()
            # print(current)
            # print(self.patternCopy)
            # print(len(self.patternCopy))
            while 1:
                ##@brief A reference time for use in utime.ticks_diff
                refTime = utime.ticks_ms()
                ##@brief A difference in time to determine how much time has passed.
                diff = utime.ticks_diff(refTime, startTime)
                if current == 'dot':
                    if diff > 100 and diff <= 500: 
                        self.t2ch1.pulse_width_percent(100)
                    elif diff > 500:
                        self.t2ch1.pulse_width_percent(0)
                        startTime = utime.ticks_ms()
                        if len(self.patternCopy) > 0:
                            current = self.patternCopy.pop(0)
                        else:
                            break
                        # print(current)
                        # print(self.patternCopy)
                        # print(len(self.patternCopy))
                    else:
                        pass
                    
                elif current == 'space':
                    if diff > 100 and diff <= 800: 
                        self.t2ch1.pulse_width_percent(0)
                    elif diff > 800:
                        startTime = utime.ticks_ms()
                        if len(self.patternCopy) > 0:
                            current = self.patternCopy.pop(0)
                        else:
                            break
                        # print(current)
                        # print(self.patternCopy)
                        # print(len(self.patternCopy))
                    else:
                        pass
                    
                elif current == 'press':
                    if diff > 100 and diff <= 1300: 
                        self.t2ch1.pulse_width_percent(100)
                    elif diff > 1000:
                        self.t2ch1.pulse_width_percent(0)
                        startTime = utime.ticks_ms()
                        if len(self.patternCopy) > 0:
                            current = self.patternCopy.pop(0)
                        else:
                            break
                        # print(current)
                        # print(self.patternCopy)
                        # print(len(self.patternCopy))
                    else:
                        pass
                    
                    
            self.transitionTo(2)
                
            
        elif self.state==2: 
            ##@brief Used to see if button press is rising or falling.
            up = 1
            # self.patternCopy = self.pattern.copy()
            # current = self.patternCopy.pop(0)
            
            diff = 0
            self.pushTime = utime.ticks_ms()
            
            while 1:
                refTime = utime.ticks_ms()
                        
                if self.buttonPushed:
                     self.buttonPushed = 0
                     if up:
                         self.upTime = utime.ticks_ms()
                         self.pushTime = utime.ticks_ms()
                         up = 0
                     elif not up:
                         self.downTime = utime.ticks_ms()
                         self.pushTime = utime.ticks_ms()
                         up = 1
                     
                
                if up:
                    buttonDiff = utime.ticks_diff(self.downTime, self.upTime)
                    #print(buttonDiff)
                    if buttonDiff > 0 and buttonDiff < 400:
                        self.userSoln.append('dot')
                        self.pushTime = utime.ticks_ms()
                        self.upTime = 0
                        self.downTime = 0
                    elif buttonDiff > 400 and buttonDiff < 1000:
                        self.userSoln.append('press')
                        self.pushTime = utime.ticks_ms()
                        self.upTime = 0
                        self.downTime = 0
                        
                diff = utime.ticks_diff(refTime, self.pushTime)
                if len(self.userSoln) > 0 and diff > 700 and diff < 1500:
                    self.userSoln.append('space')
                    self.pushTime = utime.ticks_ms()
                    
                if len(self.userSoln) >= len(self.pattern):
                    #print(self.userSoln)
                    break
                
                
                self.transitionTo(3)
                
        elif self.state==3:
            if self.userSoln == self.pattern:
                print('Win')
                userInput = input("Type CTRL-C to exit or enter to play again. ")
                self.userSoln = []
                self.difficulty += 1
                self.wins += 1
                self.transitionTo(0)
                
            elif self.userSoln != self.pattern:
                self.losses += 1
                print('Loss')
                print('You win %d games and lost %d games.' %(self.wins, self.losses))
                raise(KeyboardInterrupt)
   
                
        else:
            pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def createPattern(self, difficulty):
        '''
        @brief    Function to create the pattern
        @details  Uses a for loop to create the pattern to match the current
                  difficulty
        param    difficulty The current difficulty. 
        '''
        options = ['dot', 'press', 'space']
        pattern = []
        i = 0
        for i in range(difficulty): 
            if i != (difficulty - 1) and i != 0:
                pattern.append(random.choice(options))
            else:
                pattern.append(random.choice(options[0:2]))
        return pattern
    

    
    def transitionTo(self, newState):
        '''
        @brief   Change the current state
        param    newState The state to be changed to. 
        '''
        self.state = newState
    
    