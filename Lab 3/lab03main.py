'''@file    lab03main.py
@brief      Program that runs the Morse Finite State Machine
@details    Runs the Morse Finite State Machine which can be seen in the 
            morse.py documentation. 
            Full code for lab03main.py can be found here: 
            https://bitbucket.org/kcmcgrat/me305_labs/src/master/Lab%203/lab03main.py
                
                
    
@author     Kyle McGrath
@date       2/18/21
@copyright  License Info Here
'''

from morse import Morse


if __name__ == "__main__":
   
    ##@brief Variable to start the program when the user is ready.
    run = input("A light pattern will display, match it as best as you can using the B1 button. Press enter to begin. ")
    ##@brief Variable to contain the Morse class
    Morse = Morse()
  
    while True:
        try:
            Morse.run()
            
        except KeyboardInterrupt:
            break
        
    #Program de-initilization goes here 
