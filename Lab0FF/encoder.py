'''@file    encoder.py
@brief      The encoder driver class. 
@details    Class that contains the initialization for each encoder as well as 
            the commands for each encoder. These commands include: update, get_position, 
            set_position, and get_delta. 
    
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''


from pyb import Timer, Pin, delay



class Encoder:
    
    def __init__(self, timer, pin1, pin2):
           
        self.TIM = timer
        self.TIM.channel(1, mode=Timer.ENC_AB, pin=pin1)
        self.TIM.channel(2, mode=Timer.ENC_AB, pin=pin2)
        
        self.position = 0
        self.lastPosition = 0 
        self.totalPosition = 0
      
        
        
    def update(self):
        self.lastPosition = self.position
        self.position = self.TIM.counter() 
        
        
    def get_position(self):
        return self.position
        
        
    def set_position(self, value):
        self.position = value 
        
        
    def get_delta(self): 
        self.totalPosition += (self.position - self.lastPosition)
        return (self.position - self.lastPosition)
        
        
    