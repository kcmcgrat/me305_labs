'''@file    shares.py
@brief      A shared data file. 
@details    A shared class to allow for easier data transfer.  
    
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''


class Shares:
    
    def __init__(self):
           
        self.userIn = 0
        
    def set_input(self, value):
        self.userIn = value
        
        
