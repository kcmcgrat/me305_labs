'''@file    UIFront.py
@brief      Program that runs on Spyder as a User Interface. 
@details    Acts as a User Interface for the user to input commands. For example
            g is used to start data collection and s is used to stop data collection. 
            Encoder commands include: z to zero the encoder position, p to print 
            encoder position, and d to print the delta of the encder. 
                
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''


import keyboard
import serial
from array import array
from matplotlib import pyplot

def getData():
    ''' Example function sends a g and waits for characters back from nucleo
        then converts the characters to a string and returns it. A timeout is
        added so it doesn't wait forever to get characters if none are sent.'''
    timeout = 0
    output = array('f', [])
    while ser.in_waiting == 0:
        timeout+=1
        if timeout > 10_000_000:
            return None
 
    return 1

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global user_in
    user_in = key.name
    ser.write(user_in.encode())
    

with serial.Serial(port='COM4',baudrate=115273,timeout=1) as ser:
    times = array('f', [])
    vals = array('f', [])
    user_in  = None
    keyboard.on_release_key("s", callback=kb_cb, suppress = True)
    keyboard.on_release_key("g", callback=kb_cb, suppress = True)
    print("Enter 'g' to get data from the Nucleo or 's' to quit\r\n>>")
    while True:
        try:
            if user_in is not None:       

                if user_in == 'g' or user_in == 'G':
                    print(user_in)
                    dataString = getData()
                    if dataString:
                        cur = ser.readline().decode() 
                        while cur:
                                #print(cur)
                                stripCur = cur.strip()
                                #print(stripCur)
                                splitCur = stripCur.split(',')
                                #print(splitCur)
                                times.append(float(splitCur[0]))
                                vals.append(float(splitCur[1]))
                                cur = ser.readline().decode()
                    #if dataString != None:
                    # print('\n')
                    # print(times)
                    # print(vals)
                    last_key = None
                   
                    pyplot.figure()
                    pyplot.plot(times, vals)
                    pyplot.xlabel('Time')
                    pyplot.ylabel('Data')
                    
                    
                    file = open("CSV.txt", "w")
                    for n in range(len(times)):
                        file.write('{:}, {:}\n'.format(times[n], vals[n]))
           

                    
                elif user_in == 's' or user_in == 'S':
                    print(user_in)
                    ser.write('s'.encode())
                    print('Thanks')
                    last_key = None
                    break
                
                
                elif user_in == 'z' or user_in == 'Z':
                    print(user_in)
                    ser.write('z'.encode())
                    
                    
                    last_key = None
                    break
                
                
                elif user_in == 'p' or user_in == 'P':
                    print(user_in)
                    ser.write('p'.encode())
                    dataString = getData()
                    position = ser.readline().decode()
                    print(position)
                    last_key = None
                    break
                
                elif user_in == 'd' or user_in == 'D':
                    print(user_in)
                    ser.write('d'.encode())
                    dataString = getData()
                    delta = ser.readline().decode()
                    print(delta)
                    last_key = None
                    break

        except KeyboardInterrupt:
            break