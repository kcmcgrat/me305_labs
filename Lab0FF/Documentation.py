## \page The Term Project
#
# This is the page for the Term Project, a four week long project with the goal
# of creating a system that controls a motor while also collecting data. We used
# a Nucleo microcontroller and a motor controller PCB. There were also two encoders
# and two DC motors connected to the motor controller. 
#
#@author     Kyle McGrath
#@date       3/18/21
#@copyright  License Info Here
#
# The term project is divided in the following sections:
# - \subpage week1 
# - \subpage week2
# - \subpage week3
# - \subpage week4 
#

## \page week1 Week 1
# Week 1 Files Include: 
#
#   \ref main.py
#
#   \ref dataCollector.py
#
#   \ref UIFront.py
#
# The topic of Week 1 was creating a data collection system. It consisted of 
# Nucleo class file regarding data collection, a 'main' file for the Nucleo, and  
# a user interface that ran on Spyder. In the Nucleo class file is a finite state machine in which the user
# presses g to begin data collection for 30 seconds. The user can also press s
# in order to stop data collection at that time. After data was sampling it was 
# put into a function f = exp(-t/10)*sin(2*pi/3*time) and plotted in the user
# interface that ran on Spyder. The time and data values were also saved to a .csv
# file. 
# 
# The finite state machine can be seen below. 
# 
#@image      html Week1.jpg State-transition diagram for the system
#@image      html Week1Graph.jpg Data vs Time for 30 seconds of data collection
#@image      html Week1Graph2.jpg Data vs Time for 2.5 seconds of data collection
# 
# Click here for next week \ref week2


## \page week2 Week 2
# Week 2 Files Include: 
#
#   \ref main.py
#
#   dataCollector.py
#
#   \ref UIFront.py
#
#   \ref encoder.py
#
#   \ref encoderTask.py
# 
#   \ref shares.py
#
# The topic of Week 2 was to create an Encoder Driver Class, an Encoder Task, 
# and to add to the User Interface task from the previous week. The Encoder 
# Driver Class needed to be able to update its position, return its position, 
# set its position, and get the difference in current position and the last position
# The Encoder Task needed to call update() regularly as well as manage any encoder
# objects that are made. The Encoder Task file utilizes a finite state machine
# that changes its state based on what the user inputs in the UIFront file. 
# The UIFront file needed to be updated to allow for three new commands: zeroing 
# the encoder, printing the encoder's position, and printing out the encoder's 
# delta. I also begin to implement a shares file in order for data transfer to 
# be easier. 
#
# The finite state machine for the Encoder Task can be seen below. 
# 
#@image      html Week2.jpg State-transition diagram for the system
# 
# Click here for next week \ref week3


## \page week3 Week 3
# Week 3 Files Include: 
#
#   main.py
#
#   dataCollector.py
#
#   UIFront.py
#
#   encoder.py
#
#   encoderTask.py
#
#   \ref motorDriver.py
#
#   closedLoopTask.py
#
# The topic of Week 3 was to create code to control the motor as well as make it spin.  
# The files to be created are a Motor Driver CLass and a Closed Loop Controller Class. 
# The goal was to use the motor speed from encoder data in order to cauclate the 
# the actuation level for the motor using a user-input gain value Kp. This is around
# as far as I got done with the project before the due date. 
#
#A diagram of the transfer of information can be seen below. 
# 
#@image      html Week3.jpg Week 3 Organization. 
# 
# Click here for next week \ref week4

## \page week4 Week 4
# Week 4 Files Include: 
#
#   main.py
#
#   dataCollector.py
#
#   UIFront.py
#
#   encoder.py
#
#   encoderTask.py
#
#   motorDriver.py
#
#   closedLoopTask.py
#
# The topic of Week 4 was to create code to track the reference profile seen below. 
# I was unable to complete this task. 
#
#@image      html ReferenceProfile.jpg Reference Profile. 

