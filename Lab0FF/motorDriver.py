'''@file    motorDriver.py
@brief      The motor driver class. 
@details    Class that contains the initialization for each motor as well as the commands
            for each motor. These include: enable, disable, and set_duty. 
    
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''


from pyb import Timer, Pin, delay


class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO 
        pins and turning the motor off for safety.
        '''
        self.nSleep = nSLEEP_pin
        self.TIM = timer
        self.IN1 = TIM.channel(1, mode=TIM.PWM, pin = IN1_pin)
        self.IN2 = TIM.channel(1, mode=TIM.PWM, pin = IN1_pin)
        
        print('Creating a motor driver')
        
    def enable(self):
        self.nSleep = 1
        
        print('Enabling Motor')
        
    def disable(self):
        
        self.nSleep = 0
        print('Disabling Motor')
        
    def set_duty(self, duty):
        pass
        
        
