'''@file    closedLoopClass.py
@brief      A motor driver to perform closed-loop speed control. 
@details    Creates a closedLoop class for closed-loop speed control. Includes
            commands such as: update, get_Kp, and set_Kp. 
    
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''


class ClosedLoop:
    
    def __init__(self):
       self.Kp = 0 
        
    def update(self):
        pass
        
        
    def get_Kp(self):
        return self.Kp
        
        
    def set_Kp(self, value):
        self.Kp = value
        
 
        
    
