'''@file    dataCollector.py
@brief      FSM to collect data on the Nucleo. 
@details    Implements a finite state machine that collects data once
            the user has input a g and stops collecting data once the user has 
            input an s or once 30 seconds have passed. After data was sampling it was 
            put into a function f = exp(-t/10)*sin(2*pi/3*time) send back to the 
            UI front. 
    
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''

import random
import utime
import pyb
from pyb import UART
from array import array
from math import exp, sin, pi

class Collect:
    
    def __init__(self):
        '''
        @brief    Initialize the Collect class. 
        '''
        
        ##@brief Variable containing the state.
        self.state = 0
        ##@brief Initialize UART 
        self.myuart = UART(2)
        pyb.repl_uart(None)
        self.startTime = 0
        self.endTime = 0
        self.times = []
        self.myOut = 0
        
        
        
    def run(self, shares):
        '''
        @brief    Function that runs when the state machine runs. 
        @details  Cycles through the states of the finite state machine. 
        '''
                
        if self.state==0:
            chars_received = 0
            if self.myuart.any():
                in_char = self.myuart.read(1).decode()
                
                #chars_received += 1
            
            # If the character was a 'g' respond with a formatted string encoded as
            # a bytearray
                if in_char == 'g':
                    
                    self.state = 1
                    self.startTime = utime.ticks_ms()
                    
            
            
        elif self.state==1: 
            self.curTime = utime.ticks_ms() 
            diff = utime.ticks_diff(self.curTime, self.startTime)
            if self.myuart.any() and diff < 30000:
                val = self.myuart.read(1).decode()
                
                if val == 's':
           
                    self.endTime = utime.ticks_ms()
                    valdiff = int(utime.ticks_diff(self.endTime, self.startTime) / 10)
                    self.times  = array('f', list(time/100 for time in range(0, valdiff)))
                    self.state = 2
                    
            elif diff >= 30000:
         
                self.myOut = 3001 
                self.times = list(time/100 for time in range(0, 3001))
                self.state = 2
                    
                    
        elif self.state==2:
         
            
            for time in self.times:
                val = exp(-time/10)*sin(2*pi/3*time)
                my_out = '{:}, {:}\r\n'.format(time, val)
                self.myuart.write(my_out.encode())
                
            self.state = 3
       
                
                        
        elif self.state==3:
            pass
                        
                
   
                
        else:
            pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
 
    
    def transitionTo(self, newState):
        '''
        @brief   Change the current state.
        param    newState The state to be changed to. 
        '''
        self.state = newState
    
    