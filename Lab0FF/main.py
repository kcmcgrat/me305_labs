'''@file    main.py
@brief      The main Nucleo file. 
@details    Initializes the classes that contains Finite State Machines. Contains
            a while loop that executes the run command of those classes indefinitely. 
    
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''

from dataCollector import Collect
from shares import Shares
from EncoderTask import EncoderTask

shares = Shares()
collect=Collect()
encoders = EncoderTask()
while True:
    try:
        
        collect.run(shares)
        encoders.run(shares)
        
    except KeyboardInterrupt:
            break
        
