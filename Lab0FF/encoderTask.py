'''@file    encoderTask.py
@brief      FSM to run the encoders. 
@details    Implements a finite state machine shown below that executes encoder
            commands based on the user input. 
    
@author     Kyle McGrath
@date       3/18/21
@copyright  License Info Here
'''

import random
import utime
import pyb
from encoder import Encoder
from pyb import Timer, Pin, delay, UART


class EncoderTask:
    

    
    def __init__(self):
        '''
        @brief    Initialize the Morse class. 
      
        '''
        
        ##@brief Variable containing the state.
        self.state = 0
        self.myuart = UART(2)
        pyb.repl_uart(None)
        # Encoder 1
        self.timer1 = Timer(4, period=0xFFFF, prescaler=0)
        self.pin1 = Pin.cpu.B6
        self.pin2 = Pin.cpu.B7
        
        self.encoder1 = Encoder(self.timer1, self.pin1, self.pin2)
        
        # Encoder 2
        self.timer2 = Timer(8, period=0xFFFF, prescaler=0)
        self.pin3 = Pin.cpu.C6
        self.pin4 = Pin.cpu.C7
       
        self.encoder2 = Encoder(self.timer2, self.pin3, self.pin4) 
       
    
    def run(self, shares):
        '''
        @brief    Function that runs when the state machine runs. 
        @details  Cycles through the states of the finite state machine. 
        '''
    
        
        self.encoder1.update() 
        
        if self.state==0:
            chars_received = 0
            if self.myuart.any():
        # Read one character and turn it into a string
                in_char = self.myuart.read(1).decode()
                
                #chars_received += 1
            
            # If the character was a 'g' respond with a formatted string encoded as
            # a bytearray
                if in_char == 'z':
                    self.state = 1
                                        
                elif in_char == 'p':
                    
                    self.state = 2
                    
                elif in_char == 'd':
                    
                    self.state = 3
                    
            
        elif self.state==1:
            self.encoder1.set_position(0)
            self.state = 0 
            
        elif self.state==2:
            position = self.encoder1.get_position()
            my_out2 = 'Encoder 1 position is {:} \r\n'.format(position)
            self.myuart.write(my_out2.encode())
            self.state = 0 
            
        elif self.state==3:
            delta = self.encoder1.get_delta()
            my_out3 = 'Encoder 1 delta is {:} \r\n'.format(delta)
            self.myuart.write(my_out3.encode())
            self.state = 0 
            
 
    def transitionTo(self, newState):
        '''
        @brief   Change the current state.
        param    newState The state to be changed to. 
        '''
        self.state = newState
    
    